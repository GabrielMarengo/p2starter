package edu.uprm.ece.icom4035.list;

import java.util.Iterator;
import java.util.NoSuchElementException;


/**
 * @author Gabriel Marengo
 */
public class SortedCircularDoublyLinkedList<E extends Comparable<E>> implements SortedList<E> {
	Node header;
	int currentSize;
	
	public SortedCircularDoublyLinkedList(){
		header = new Node();
		header.setElement(null);
		header.setNext(header);
		header.setPrev(header);
		currentSize = 0;
}
	
	/**
	 * Private Node class
	 */
	private class Node {
		private E element;
		private Node next;
		private Node prev;

		public E getValue() {
			return element;
		}
		
		public void setElement(E element) {
			this.element = element;
		}
		
		public Node getNext() {
			return next;
		}
		
		public void setNext(Node next) {
			this.next = next;
		}
		
		public Node getPrev() {
			return prev;
		}
		
		public void setPrev(Node prev) {
			this.prev = prev;
		}
	}
	
	/**
	 * Private forward iterator
	 */
	private class ListIterator implements Iterator<E>{
		private Node nextNode;

		public ListIterator(){
			this.nextNode = header.getNext();
		}

		public ListIterator(int index){
			if((index < 0) || (index>currentSize))
				throw new IndexOutOfBoundsException();

			int counter = 0;
			
			for(this.nextNode=header.getNext();counter<index;nextNode=nextNode.getNext(),counter++);
		}
		
		@Override
		public boolean hasNext() {
			return nextNode.getValue() != null;
		}
		
		@Override
		public E next() {
			if (hasNext()){
				E result = this.nextNode.getValue();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
		
		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		} 

	} //End of List Iterator
	
	/**
	 * private backward iterator
	 *
	 */
	private class ReverseListIterator implements ReverseIterator<E>{
		private Node prevNode;
		
		public ReverseListIterator(){
			this.prevNode = header.getPrev();
		}
		
		public ReverseListIterator(int index){
			if(index<0 || index>=currentSize){
				throw new IndexOutOfBoundsException();
			}
			int counter=0;
			for(this.prevNode=header.getNext();counter<index;this.prevNode=this.prevNode.getNext(),counter++);
		}
		
		@Override
		public boolean hasPrevious() {
			// TODO Auto-generated method stub
			return this.prevNode!=header;
		}
		
		@Override
		public E previous() {
			if (hasPrevious()){
				E result = prevNode.getValue();
				prevNode = prevNode.getPrev();
				return result;
			}
			else
				throw new NoSuchElementException();	
		}	
	}

	/**
	 * Iterator method
	 */
	@Override
	public Iterator<E> iterator() {
		return new ListIterator();
	}
	
	/**
	 * add method
	 * @param obj element to be added
	 * @throws IllegalArgumentException if the param is null
	 */
	@Override
	public boolean add(E obj) {
		//checks if obj is null
		if(obj == null)
			throw new IllegalArgumentException("Obj cannot be Null");
		
		if(this.currentSize==0){
			Node newNode=new Node();
			newNode.setElement(obj);
			this.header.setNext(newNode);
			this.header.setPrev(newNode);
			newNode.setPrev(this.header);
			newNode.setNext(this.header);
			this.currentSize++;
			return true;
		}else{
			Node temp=null;
			for(temp=this.header.getNext();temp!=this.header;temp=temp.getNext()){
				if(obj.compareTo(temp.getValue())<=0){
					Node newNode=new Node();
					newNode.setNext(temp);
					newNode.setPrev(temp.getPrev());
					newNode.setElement(obj);
					temp.getPrev().setNext(newNode);
					temp.setPrev(newNode);
					this.currentSize++;
					return true;
				}
			}
			if(temp==this.header){
				Node newNode=new Node();
				newNode.setNext(temp);
				newNode.setPrev(temp.getPrev());
				newNode.setElement(obj);
				temp.getPrev().setNext(newNode);
				temp.setPrev(newNode);
				this.currentSize++;
				return true;
			}
		}
		return false;
	}

	/**
	 * size() method
	 * @return the size of the list
	 */
	@Override
	public int size() {
		return currentSize;
	}

	/**
	 * Remove method
	 * removes the first time an obj appears on the list
	 * @return true if it found the an isntance of the obj and removed it
	 * @param the obj to be removed
	 * @throws IllegalArgumentException if the param is null
	 */
	@Override
	public boolean remove(E obj) {
		if(obj == null)
			throw new IllegalArgumentException("Parameter cannot be null");
		
		Node temp=null;
		for(temp=this.header.getNext();temp!=this.header;temp=temp.getNext()){
			if(temp.getValue().equals(obj)){
				temp.getPrev().setNext(temp.getNext());
				temp.getNext().setPrev(temp.getPrev());
				temp.setNext(null);
				temp.setPrev(null);
				temp.setElement(null);
				this.currentSize--;
				return true;
			}
		}
		return false;
	}

	/**
	 * Remove method
	 * removes the obj at the specified location
	 * @return true if the obj is removed
	 * @param int index where the obj is that will be removed
	 * @throws IndexOutOfBoundsException if the param is not between 0 and the size
	 */
	@Override
	public boolean remove(int index) {
		if(index < 0 || index > this.size())
			throw new IndexOutOfBoundsException("The index must be between 0 and the size of the array");
		
		int counter=0;
		Node temp=null;
		
		for(temp=this.header.getNext();counter<index;temp=temp.getNext(),counter++);
		temp.getPrev().setNext(temp.getNext());
		temp.getNext().setPrev(temp.getPrev());
		temp.setElement(null);
		temp.setNext(null);
		temp.setPrev(null);
		this.currentSize--;
		return true;
	}

	/**
	 * RemoveAll method
	 * removes obj from the list
	 * @return the amount of obj it removed from the list
	 * @param the obj to be removed
	 */
	@Override
	public int removeAll(E obj) {
		int count = 0;
		while(this.remove(obj))
			count++;
		
		return count;
	}

	/**
	 * first method
	 * @return the first element of the list
	 */
	@Override
	public E first() {
		if(this.isEmpty())
			return null;
		return header.getNext().getValue();
	}

	/**
	 * last method
	 * @return the last element of the list
	 */
	@Override
	public E last() {
		if(this.isEmpty())
			return null;
		return header.getPrev().getValue();
	}

	/**
	 * get method
	 * @return the element at the desired position
	 * @param the index where the element is to be searched
	 * @throws IndexOutOfBoundsException if the param is not between 0 and the size
	 */
	@Override
	public E get(int index) {
		if(index < 0 || index > this.size())
			throw new IndexOutOfBoundsException("The index must be between 0 and the size of the array");
		
		Node temp=this.header.getNext();
		for(int counter=0;counter<index;counter++,temp=temp.getNext());
		
		return temp.getValue();
	}
	
	/**
	 * clear method
	 * Clears the entire list
	 */
	@Override
	public void clear() {
		while(!this.isEmpty()){
			this.remove(0);
		}
		
	}

	/**
	 * contains method
	 * @return true if it contains e
	 * @param the element that will be searched for in the list
	 * @throws IllegalArgumentException if the param is null
	 */
	@Override
	public boolean contains(E e) {
		if(e == null)
			throw new IllegalArgumentException("e cannot be null");
		for(Node temp = header.getNext(); temp != header; temp = temp.getNext())
			if(temp.getValue() == e)
				return true;
		
		return false;
	}

	/**
	 * isEmpty method
	 * verifies if the list is empty
	 * @return true if size = 0
	 */
	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public Iterator<E> iterator(int index) {
		return new ListIterator(index);
	}

	/**
	 * firstIndex method
	 * @return the index of the first place it found e, -1 if e is not in the list
	 * @throws IllegalArgumentException if the param is null
	 */
	@Override
	public int firstIndex(E e) {
		if(e == null)
			throw new IllegalArgumentException("e cannot be null");
		
		int count = 0;
		for(Node temp = header.getNext(); temp != header; count++, temp = temp.getNext())
			if(temp.getValue() == e)
				return count;
		
		return -1;
	}

	/**
	 * lastIndex method
	 * @return the index of the lst place it found e, -1 if e is not in the list
	 * @throws IllegalArgumentException if the param is null
	 */
	@Override
	public int lastIndex(E e) {
		if(e == null)
			throw new IllegalArgumentException("e cannot be null");
		
		int count = this.size()-1;
		for(Node temp = header.getPrev(); temp != header; count++, temp = temp.getPrev())
			if(temp.getValue() == e)
				return count;
		
		return -1;
	}

	@Override
	public ReverseIterator<E> reverseIterator() {
		return new ReverseListIterator();
	}

	@Override
	public ReverseIterator<E> reverseIterator(int index) {
		return new ReverseListIterator(index);
	}

	
	
	
	
}
